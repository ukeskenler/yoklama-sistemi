$(function(){
    $('.new-student').on('click',function(){
        $('input[name=tc]').focus();
        $.get('/student/create',function(res){
            $('#student-form tbody').html(res);
        },'html');
    });

    $('.form-close').on('click',function(){
        $('.student-form').hide();
    });

    $.get('/student/list',function(res){
        $('.student-list').append(res);
        var list_total_count = $('.student-list tr').length;
        $('.list-total-count').html(list_total_count);

    });

    $(document).on('submit','#student-form',function(e){
        e.preventDefault();
        var data = $(this).serialize();
        $.post('/student',data,function(res){
            $('.student-list').prepend(res);
            $('#student-form').get(0).reset();
            $('input[name=tc]').focus();
        });
    })

});
