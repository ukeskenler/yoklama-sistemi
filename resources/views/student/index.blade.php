@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header ">
                        <div class="d-flex justify-content-between">
                            <h5>{{ __('Öğrenciler') }}</h5>
                            <div>
                                <a class="btn btn-primary btn-sm new-student" href="#"><i class="bi bi-clipboard-plus"></i> Yeni</a>
                            </div>
                        </div>
                    </div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        <div class="list-group">
                            <label class="list-group-item d-flex gap-3">
                                <div class="d-flex justify-content-between w-100">
                                    <div>
                                    <input class="form-check-input me-3" type="checkbox" value="" checked="">
                                    <span class="pt-1 form-checked-content d-inline-block">
                                        <strong>Muhammet Uğur Keskenler</strong>
                                        <small class="d-block text-muted">
                                            12345678901 - 05376104845
                                        </small>
                                    </span>
                                    </div>
                                    <img src="{{ asset('img/user.png') }}" width="50">
                                </div>
                            </label>
                        </div>
                        <table class="table table-striped table-hover">
                            <thead>
                                <tr>
                                    <th scope="col"><span class="list-total-count"></span></th>
                                    <th scope="col">TC</th>
                                    <th scope="col">Ad Soyad</th>
                                    <th scope="col">Telefon</th>
                                    <th scope="col">İşlemler</th>
                                </tr>
                            </thead>
                            <form id="student-form">
                                <tbody class="bg-secondary student-form"></tbody>
                            </form>
                            <tbody class="student-list"></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
