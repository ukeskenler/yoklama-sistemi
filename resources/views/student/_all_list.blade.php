@if(!empty($students))
    <?php $i=1; ?>
    @foreach($students as $student)
        <tr class="student" data-id="{{ $student->id }}" data-tc="{{ $student->tc }}" data-name="{{ $student->name }}" data-surname="{{ $student->surname }}" data-phone="{{ $student->phone }}">
            <th scope="row">{{ $i }}</th>
            <td>{{ $student->tc }}</td>
            <td>{{ $student->name.' '.$student->surname }}</td>
            <td>{{ $student->phone }}</td>
            <td>@mdo</td>
        </tr>
        <?php $i++; ?>
    @endforeach
@endif

