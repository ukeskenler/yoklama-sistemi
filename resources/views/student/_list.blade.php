<tr class="student bg-success bg-opacity-10 text-dark" data-id="{{ $student->id }}" data-tc="{{ $student->tc }}" data-name="{{ $student->name }}" data-surname="{{ $student->surname }}" data-phone="{{ $student->phone }}">
    <th scope="row">-</th>
    <td>{{ $student->tc }}</td>
    <td>{{ $student->name.' '.$student->surname }}</td>
    <td>{{ $student->phone }}</td>
    <td>@mdo</td>
</tr>

