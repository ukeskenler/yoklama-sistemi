<style>
  .room{
    text-decoration: none;
    opacity: 1;
    transition: all 250ms;
  }
  .room:hover{
    opacity: 0.7;
  }
  .bg-red{
    background-color: #792017 !important;
  }
  .bg-green{
    background-color: #17795e !important;
  }
</style>

@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <?php
                    $bgcolor = [
                      'A' => 'primary',
                      'B' => 'secondary',
                      'C' => 'success',
                      'D' => 'info',
                      'E' => 'green',
                      'F' => 'red',
                    ];
                     ?>

                    <main>

                      <section class="py-5 px-4 text-center container">
                        <div class="row">
                          <div class="col-lg-6 col-md-8 mx-auto">
                            <h1 class="fw-light">Odalar</h1>
                            <!--
                            <p>
                              <a href="#" class="btn btn-primary my-2">Oda Ekle</a>
                              <a href="#" class="btn btn-secondary my-2">Oda Düzenle</a>
                            </p>
                          -->
                          </div>
                        </div>
                      </section>

                      <div class="album py-5 bg-light">
                        <div class="container">
                        <?php foreach(range('A', 'F') as $letter): ?>
                          <div class="row mb-3">
                            <?php for($i=1;$i<9;$i++): ?>
                                <div class="col">
                                  <div class="card shadow-sm">
                                  <a href="" class="room" data-letter="<?=$letter?>" data-room="<?=$i?>" data-bs-toggle="modal" data-bs-target="#staticBackdrop">
                                    <div class="d-flex flex-column justify-content-center align-items-center bg-<?=$bgcolor[$letter]?> bg-gradient py-4 text-white">
                                        <span><?=$letter?><?=$i?></span>
                                            </div>
                                  </a>

                                    <div class="card-body">
                                      <div class="d-flex justify-content-between align-items-center">
                                        <div class="btn-group mx-auto">
                                          <button type="button" class="btn btn-sm btn-outline-secondary"><i class="fas fa-home"></i></button>
                                          <button type="button" class="btn btn-sm btn-outline-secondary"><i class="fas fa-users"></i></button>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                            <?php endfor;  ?>
                          </div>
                          <?php endforeach; ?>
                        </div>
                      </div>
                    </main>
                    {{ __('You are logged in!') }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection


<!-- Modal -->
<div class="modal fade" id="staticBackdrop" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="staticBackdropLabel"><strong class="room-name"></strong></h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body" id="list-modal">

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary btn-sm" data-bs-dismiss="modal">Kapat</button>
      </div>
    </div>
  </div>
</div>




<script>
$(function(){
  $('.room').on('click',function(){
    var letter = $(this).attr('data-letter');
        room   = $(this).attr('data-room');

    $('.room-name').html(letter+room);

    $.post('<?=site_url()?>/home/room_users',{'letter':letter,'room':room},function(res){
      $('#list-modal').html(res);
    });
  });
});
</script>
