
<div class="count-alert"></div>
<div class="list-group">
  <?php foreach($users as $user): ?>

  <div class="list-group-item list-group-item-action" >
    <span class="d-inline-block mt-2"><?=$user->ad.' '.$user->soyad; ?></span>
    <div class="btn-group float-end" role="group">
      <input type="checkbox" class="chktoggle " checked data-width="72" data-user="<?=$user->user_id?>" data-room="<?=$user->oda?>" data-toggle="toggle" data-on="Burada" data-off="Yok" data-size="sm" data-onstyle="success" data-offstyle="secondary">
    </div>
  </div>

    <?php endforeach; ?>
</div>
<link rel="stylesheet" href="<?=base_url('assets/toggle/bootstrap-toggle.min.css');?>">
<script src="<?=base_url('assets/toggle/bootstrap-toggle.min.js');?>"></script>
<script>
  $(function(){
    $('.chktoggle').bootstrapToggle()
    $('.chktoggle').on('change',function(){
        var user = $(this).attr('data-user');
            room = $(this).attr('data-room');
            checked = $(this).prop('checked');

        $.post("<?=site_url('home/user_count')?>",{'user':user,'oda':room,'checked':checked},function(res){
            if(res.message == true){
                $('.count-alert').html('<div class="alert alert-success">Yoklama alındı!</div>');
            }else{
                $('.count-alert').html('<div class="alert alert-danger">Hata oluştu!</div>');
            }
            console.log(res);
        },'json');

    });
  });
</script>
