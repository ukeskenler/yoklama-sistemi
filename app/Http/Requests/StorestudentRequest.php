<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StorestudentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'      => 'required',
            'surname'   => 'required',
            'phone'     => 'required',
            'tc'        => 'nullable|unique:students'
        ];
    }
    public function messages()
    {
        return [
            //'phone.required' => 'Telefon alanı boş geçilemez',
            //'name.required' => 'İsim alanı boş geçilemez',
            //'surname.required' => 'Soyisim alanı boş geçilemez',
            //'phone.unique' => 'Girilen telefon daha önce kayıtedilmiş',
        ];
    }
}
